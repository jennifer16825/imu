-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: imu_imu
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branch_details`
--

DROP TABLE IF EXISTS `branch_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branch_details` (
  `branch_id` int(100) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) NOT NULL,
  `branch_status` enum('active','inactive','','') NOT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch_details`
--

LOCK TABLES `branch_details` WRITE;
/*!40000 ALTER TABLE `branch_details` DISABLE KEYS */;
INSERT INTO `branch_details` VALUES (1,'IMU-Chennai','active'),(2,'IMU-Kolkatta','active'),(3,'IMU-Kochi','active'),(4,'IMU-Navi-mumbai','active'),(5,'IMU-Mumbai-port','active'),(6,'IMU-Vishakapatnam','active');
/*!40000 ALTER TABLE `branch_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dynamic_content`
--

DROP TABLE IF EXISTS `dynamic_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dynamic_content` (
  `dynamic_content_id` int(100) NOT NULL AUTO_INCREMENT,
  `module_parent_id` int(100) NOT NULL COMMENT 'module id of module_management table',
  `dynamic_content_title` varchar(255) NOT NULL COMMENT 'title',
  `title_as_link` enum('yes','no') NOT NULL COMMENT 'if the file type is link ',
  `dynamic_sub_required` enum('yes','no') NOT NULL COMMENT 'to list the main title for which we have mentioned as sub',
  `dynamic_content_type` enum('main','sub') NOT NULL COMMENT 'content type',
  `dynamic_content_reference_id` int(100) NOT NULL COMMENT 'to know the parent and child',
  `inline_file_image_need` enum('yes','no') NOT NULL COMMENT 'if yes content and image should be added in the dynamic_content field else check the file type',
  `dynamic_content` longtext NOT NULL COMMENT 'content',
  `dynamic_content_file_type` enum('pdf','image','video','doc','link') NOT NULL COMMENT 'type of the content file',
  `dynamic_content_image_path` varchar(255) NOT NULL COMMENT 'path of the dynamic content file type',
  `sub_display_on_main` enum('yes','no') NOT NULL COMMENT 'If yes then sub will be displayed on the main page else main page alone is displayed',
  `link_page_parent_id` int(50) NOT NULL COMMENT 'List all the pages which is defined as link ',
  `user_parent_id` int(50) NOT NULL COMMENT 'parent id of the user management table and to define which user made the changes',
  `branch_parent_id` int(100) NOT NULL COMMENT 'parent id of the branch_details table and from which branch the changes is done',
  `dynamic_date` date DEFAULT NULL COMMENT 'date',
  `dynamic_closing_datetime` datetime DEFAULT NULL COMMENT 'closing datetime',
  `dynamic_opening_datetime` datetime DEFAULT NULL COMMENT 'opening datetime',
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'date at which the dynamic title is created',
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'date at which the dynamic title is updated',
  `dynamic_content_status` enum('active','inactive') NOT NULL COMMENT 'status of the dynamic content',
  PRIMARY KEY (`dynamic_content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dynamic_content`
--

LOCK TABLES `dynamic_content` WRITE;
/*!40000 ALTER TABLE `dynamic_content` DISABLE KEYS */;
INSERT INTO `dynamic_content` VALUES (1,20,'main','yes','yes','main',0,'yes','main dynamic content','image','img_dynamic/flower.jpg','yes',0,0,5,NULL,NULL,NULL,'2022-04-20 08:16:30','2022-04-20 08:16:30','active'),(2,1,'course main','yes','yes','main',0,'yes','','image','img_dynamic/flower.jpg','yes',0,0,5,NULL,NULL,NULL,'2022-04-20 08:24:35','2022-04-20 08:24:35','active'),(3,1,'Under Graduate Programmes','yes','no','sub',2,'yes','Course 	Duration\r\nB.Tech (Marine Engineering) 	4 Years\r\nB.Tech (Naval Architecture & Ocean Engineering) 	4 Years\r\nB.Sc (Nautical Science) 	3 Years\r\nBBA (Logistics, Retailing & E-Commerce) 	3 Years\r\nApprenticeship embedded BBA - Maritime Logistics 	3 Years\r\nDiploma in Nautical Science leading to B.Sc. (Applied Nautical Science) 	1 Year\r\nB.Sc (Ship Building & Repair)**** 	3 Years\r\n**** Note: This Programme is being conducted only in the IMU Affiliated Institute.','','','yes',0,0,4,NULL,NULL,NULL,'2022-04-20 08:25:33','2022-04-20 08:25:33','active'),(4,1,'Post Graduate Programmes','yes','no','sub',2,'yes','Course 	Duration\r\nMBA (Port and Shipping Management) 	2 Years\r\nMBA (International Transportation and Logistics Management) 	2 Years\r\nM.Tech (Naval Architecture and Ocean Engineering) 	2 Years\r\nM.Tech (Dredging and Harbour Engineering) 	2 Years\r\nM.Tech (Marine Engineering & Management) 	2 Years\r\n\r\nPostgraduate Diploma Programme\r\nPGDME (Post Graduate Diploma in Marine Engineering) 	1 Year','','','yes',0,0,6,NULL,NULL,NULL,'2022-04-20 08:27:17','2022-04-20 08:27:17','active'),(5,1,'Research Programmes','yes','no','sub',2,'yes','PhD/MS(By Research) Programme Batch I Details','pdf','img_dynamic/PhDMS(By Research_batch1_30112021.pdf','yes',0,0,1,NULL,NULL,NULL,'2022-04-20 08:28:37','2022-04-20 08:28:37','active'),(6,1,'Schools','yes','no','sub',2,'yes','Indian Maritime university offers the above mentioned courses under the following Schools\r\n\r\nSchool of Nautical Studies\r\nSchool of Naval Architecture & Ocean Engineering\r\nSchool of Marine Engineering & Technology\r\nSchool of Maritime Management','pdf','img_dynamic/IWAI_Corrigendum-I_30032022.pdf','yes',0,0,2,NULL,NULL,NULL,'2022-04-20 08:31:01','2022-04-20 08:31:01','active'),(7,1,'course dynamic sub5','yes','no','sub',2,'yes','course dynamic sub4 content','image','img_dynamic/flower4.jpg','yes',0,0,5,NULL,NULL,NULL,'2022-04-20 08:32:28','2022-04-20 08:32:28','active'),(8,2,'event dynamic main','yes','yes','main',0,'yes','','image','img_dynamic/flower3.jpg','yes',0,0,1,NULL,NULL,NULL,'2022-04-20 08:34:03','2022-04-20 08:34:03','active'),(9,2,'Participation of Indian Maritime University (IMU) at \'Waterways Conclave 2022\' on 11th-12th April held in Dibrugarh, Assam.','yes','no','sub',8,'yes','The Hon\'ble Union Minister of Ports, Shipping & Waterways Shri. Sarbananda Sonowal visited IMU\'s booth/stall and was seen interacting with the Students of Dibrugarh in the presence of Vice-Chancellor of IMU Dr (Ms) Malini V Shankar','image','img_dynamic/Waterways Conclave 2022_2.jpg','yes',0,0,6,NULL,NULL,NULL,'2022-04-20 08:35:58','2022-04-20 08:35:58','active'),(10,2,'Waterways Conclave 2022','yes','no','sub',8,'yes','Dr. (Mrs.) Malini V Shankar, IAS (Retd.), Vice Chancellor, Indian Maritime University delivered Keynote Address in the Waterways Conclave 2022 held at Dibrugarh, Assam','image','img_dynamic/VC_Dibrugarh_Event_12042022.jpg','yes',0,0,4,'2022-04-11',NULL,NULL,'2022-04-20 08:38:31','2022-04-20 08:38:31','active'),(11,2,'TRANSTECH 2022','yes','no','sub',8,'yes','Dr. (Mrs.) Malini V Shankar, IAS (Retd.), Vice Chancellor, Indian Maritime University inaugurated the Simulator Centre and delivered the Chief Guest Address in the Transtech 2022 event organized by Tolani Maritime Institute, Pune on 26.03.2022','image','img_dynamic/Transtech_2022_1_08042022.jpg','yes',0,0,3,'2022-04-08',NULL,NULL,'2022-04-20 08:39:32','2022-04-20 08:39:32','active'),(12,2,'IMU and Logistics Sector Skill Council enters in to an MoU for introducing stipendiary apprenticeship-embedded BBA Degree in Maritime Logistics ','yes','no','sub',8,'yes','Indian Maritime University and Logistics Sector Skill Council have signed an MoU for introducing stipendiary apprenticeship-embedded BBA Degree in Maritime Logistics at the Visakhapatnam Campus of the University. The Degree will be introduced from the ensuing academic year 2022-23. The degree curriculum is embedded with two spells of apprenticeship for 12 months during the third year of study with a monthly stipend of Rs.7,500 per student. The Degree curriculum has been developed by industry experts and is focused on making the graduates industry-ready with relevant Knowledge, Skill and Attitude','image','img_dynamic/IMU-LSC Collaboration_24032022.jpg','yes',0,0,6,'2022-03-24',NULL,NULL,'2022-04-20 08:40:26','2022-04-20 08:40:26','active'),(13,2,'Maritime Education – History, Scope and Opportunities','yes','no','sub',8,'yes','Dr. (Mrs.) Malini V Shankar, IAS (Retd.), Vice Chancellor, Indian Maritime University authored an article on “Maritime Education – History, Scope and Opportunities (in Tamil)\" published in Yojana – Thittam (Feb 2022 issue), a monthly magazine of Publications Division, Ministry of Information and Broadcasting, Government of India.\r\nTheme: Higher Education (New Education Policy 2020)','image','img_dynamic/VCs_08_February_2022_Thittam_IMU.jpg','yes',0,0,1,'2022-02-08',NULL,NULL,'2022-04-20 08:41:42','2022-04-20 08:41:42','active'),(14,4,'faq dynamic main','yes','yes','main',0,'yes','','image','img_dynamic/flower.jpg','yes',0,0,2,NULL,NULL,NULL,'2022-04-20 08:52:44','2022-04-20 08:52:44','active'),(15,4,'1. What is Transcript and how to apply for Transcript Certificate?','yes','no','sub',14,'yes','A transcript is documentation of a student\'s permanent academic record, which usually means all courses taken, all grades received and degrees conferred to a student.\r\nAn application along with the online payment fee of Rs.1,000/- (+ 18% GST) (Every extra copy Rs.500/- (+ 18% GST)) may be sent to the Controller of Examinations, IMU.\r\nTime Period for issue: IMU ordinarily will take minimum 15 working days for issue of the certificate.\r\nThe online payment for Transcript Certificate can be made through IMU\'s website www.imu.ac.in under the heading Academics - \"Online payment of Academic Fee\" click Transcript Fee\r\n\r\nAbove Procedure is also applicable for Attestation of Certificates / Mark sheets.','','','yes',0,0,1,NULL,NULL,NULL,'2022-04-20 08:53:52','2022-04-20 08:53:52','active'),(16,4,'2. Procedure to send the academic records attested by the IMU along with duly filled application format directly to WES, Canada or to any Foreign Agency by the IMU?','yes','no','sub',14,'yes','Academic request form in the prescribed format shared by WES, Canada or any other foreign agency duly signed by the students.\r\nAll Academic records [Certificates, mark statements issued by IMU] to be attested by the IMU.\r\nFees for attestations for one copy Rs.1180 [inclusive of GST]\r\nPostal charges of Rs.1534 [inclusive of GST]\r\nPostal address to which it has to be sent shall be clearly mentioned.\r\nPayment should be made online through SBI i-collect available on IMU\'s website. Link is given below\r\nhttps://www.imu.edu.in//index.php?prod_id=408\r\nAfter making payment an application/mail enclosing the above documents should be sent to the Controller of Examinations, IMU Hqrs, through E-mail I.D. coe@imu.ac.in along with a proof of the payment.\r\nTime Period for issue:IMU will take minimum 15 working days for attestation/dispatch to the concerned Foreign agency.','','','yes',0,0,2,NULL,NULL,NULL,'2022-04-20 08:54:46','2022-04-20 08:54:46','active'),(17,4,'3. What is a Migration Certificate and how do I apply for the same?','yes','no','sub',14,'yes','Migration Certificate is a document issued to students when they wish to pursue higher studies in other Universities. An application should be sent to the Controller of Examinations, IMU along with the online payment fee receipt of Rs.500/- (+ 18% GST) with the Registration Number of Cadet.\r\nThe online payment can be made through IMU website www.imu.ac.in under the heading Academics - \"click online payment of academic fees\" click Migration Certificate Fee.\r\nTime Period for issue: IMU ordinarily will take minimum 15 working days for issue of the certificate.\r\nAbove mentioned fees is as per IMU Circular No.2008 dated 18.09.2020 which may be amended in future. ','','','yes',0,0,1,NULL,NULL,NULL,'2022-04-20 08:55:47','2022-04-20 08:55:47','active'),(18,4,'4. How to apply for the 3, 4, 5 Semester Examinations of the DNS leading to B.Sc (Applied Nautical Science) programme?','yes','no','sub',14,'yes','\r\n\r\nThe 3, 4, 5 Semester Examinations of the DNS leading to B.Sc (Applied Nautical Science) programme will be conducted four times in a year i.e. March, June, September and December. The student can register through their login for any examination throughout the year by paying the relevant fees in the mode as specified by the University when the examination instruction and time table are published on IMU\'s website:-\r\nOne time Re-Registration Fee for B.Sc (Applied Nautical Science - DLP)\r\n      w.e.f. September 2020	-\r\n\r\n	Rs.30,000/-\r\n     (As per Circular No. 2006 dated 07.08.2020)\r\nEach Written paper	- 	Rs.4,000/- per semester\r\nFees for mark statement	- 	Rs.150/-\r\nProvisional Certificate	- 	Rs.250/-\r\nConsolidated Mark Statement	- 	Rs.500/-\r\nDegree Certificate	- 	Rs.500/-\r\nFor arrear paper	- 	Rs.500/- for each paper\r\nMark Statement	- 	Rs.150/-\r\nThe Centre for the examination must be selected while registering for the Examination. The online Application can be applied directly by the students by login to Student Portal. In Case, Photo is not displayed, you can send a recent colour through email addressed to dlp@imu.ac.in.\r\nAbove mentioned fees is as per IMU Circular No.2008 dated 18.09.2020 which may be amended in future.\r\n\r\n','image','img_dynamic/flower5.jpg','yes',0,0,2,NULL,NULL,NULL,'2022-04-20 08:56:27','2022-04-20 08:56:27','active'),(19,4,'5. When can I apply for 3rd, 4th and 5th Semester Examinations?','yes','no','sub',14,'yes','Immediately after the completion of evaluation of SSTP by the Institute / Campus and uploading of the marks of the same in IMU\'s Examination Portal. The candidates are to note that they have to apply for 3rd, 4th and 5th Semester Examinations prior to the appearance in 6th Semester Examinations i.e. Second Mate CoC Examinations conducted by DG, Shipping. If anyone doesn\'t apply for the 3rd, 4th and 5th Semester Examinations prior to appearance of the Second Mate Examinations, IMU shall not consider the Second Mate Examinations marks and will not award the degree. ','','','yes',0,0,6,NULL,NULL,NULL,'2022-04-20 08:57:09','2022-04-20 08:57:09','active'),(20,3,'ACADEMIC BROCHURE 2022-23','yes','no','main',0,'yes','','pdf','img_dynamic/Academic_Brochure_2022-23_30032022.pdf','yes',0,0,5,NULL,NULL,NULL,'2022-04-20 09:10:35','2022-04-20 09:10:35','active'),(21,6,'alumni dynamic main','yes','yes','main',0,'yes','','image','img_dynamic/flower6.jpg','yes',0,0,5,NULL,NULL,NULL,'2022-04-20 09:14:50','2022-04-20 09:14:50','active'),(22,6,'alumni dynamic sub1','yes','no','sub',21,'yes','alumni dynamic sub1 content','image','img_dynamic/flower7.jpg','yes',0,0,3,NULL,NULL,NULL,'2022-04-20 09:16:39','2022-04-20 09:16:39','active'),(23,6,'alumni dynamic sub2','yes','no','sub',21,'yes','alumni dynamic sub2 content','image','img_dynamic/flower8.jpg','yes',0,0,6,NULL,NULL,NULL,'2022-04-20 09:21:21','2022-04-20 09:21:21','active'),(24,6,'alumni dynamic sub3','yes','no','sub',21,'yes','alumni dynamic sub3 content','image','img_dynamic/flower5.jpg','yes',0,0,6,NULL,NULL,NULL,'2022-04-20 09:27:33','2022-04-20 09:27:33','active'),(25,6,'alumni dynamic sub4','yes','no','sub',21,'yes','alumni dynamic sub4 content','image','img_dynamic/flower5.jpg','yes',0,0,5,NULL,NULL,NULL,'2022-04-21 01:36:31','2022-04-21 01:36:31','active'),(26,6,'alumni dynamic sub5','yes','no','sub',21,'yes','alumni dynamic sub4 content','image','img_dynamic/flower8.jpg','yes',0,0,2,NULL,NULL,NULL,'2022-04-21 01:37:53','2022-04-21 01:37:53','active'),(27,5,'STUDENTS ACHIEVEMENTS','yes','yes','main',0,'yes','','','','yes',0,0,2,NULL,NULL,NULL,'2022-04-21 01:43:08','2022-04-21 01:43:08','active'),(28,5,'2021 BIC Scholarship Awards','yes','no','sub',27,'yes','Cadet Purbitta Datta, a B.Tech(Marine) Alumni of IMU Kolkata Campus and Cadet Arnab Ganguly, 3rd year B.Sc.(NS), IMU Chennai Campus have won the 2021 BIC Scholarship awards offered by the Bureau International des Containers et du Transport Intermodal, Paris, France.\r\n\r\nVice Chancellor\'s Congratulatory Message:\r\n\r\nCongratulations to the award winners Ms.Purbitta Datta & Mr.Arnab Ganguly','image','img_dynamic/PurbittaDutta.png','yes',0,0,2,'2022-04-20',NULL,NULL,'2022-04-21 01:45:38','2022-04-21 01:45:38','active'),(29,5,'Vice Chancellor @ IMU Kolkata Campus','yes','no','sub',27,'yes','Dr. (Mrs.) Malini V Shankar, IAS (Retd.), Vice Chancellor, Indian Maritime University interacted with Student Organizers, Shipping, Port and Govt. dignitaries at IMU Kolkata Campus.\r\nCadet Sai Sundar Pranav, 2nd Year cadet from IMU Kolkata Campus presented a portrait of the Vice Chancellor','image','img_dynamic/VC_Kolkata_13042022.png','yes',0,0,4,'2022-04-12',NULL,NULL,'2022-04-21 01:50:25','2022-04-21 01:50:25','active'),(30,5,'Samudra Manthan 2022','yes','no','sub',27,'yes','Name of the Students & their Achievements:\r\n\r\nEnergex (Case Study)\r\n1st Position\r\nSanurag Saha\r\nRiya Bhandari\r\nPriyanshu Banerjee\r\nAman Verma\r\n\r\nOceania (Quiz)\r\nSanurag Saha - 1st Position\r\nPriyanshu Banerjee - 2nd Position\r\nSatwik Dixit - 3rd Position\r\n\r\nBeyondQ (Puzzles, Codes, and Crosswords)\r\nBadrid Doja - 1st Position\r\nSatwik Dixit - 3rd Position\r\n\r\nPaper Prep\r\nSubham Chakrabarti - 1st Position\r\nShweta Jodha & Arundhati Arul - 2nd Position\r\n\r\nAquaMOD (Ship Remodelling), Saveoic (Problem Analyzing), Energex (Case Study)\r\n2nd Position\r\nAmravathi Siddarth\r\nNaman Singh\r\nPrashant Kumar\r\nShubham Kumar\r\n\r\nOrganized By: Department of Ocean Engineering and Naval Architecture of the Indian Institute of Technology, Kharagpur (IIT-KGP)\r\n\r\nCongratulations to the students for their achievement in the event.','image','img_dynamic/SamudraManthan_2022_08042022.png','yes',0,0,3,'2022-04-08',NULL,NULL,'2022-04-21 01:52:14','2022-04-21 01:52:14','active'),(31,5,'Transtech\'22','yes','no','sub',27,'yes','Name of the Students:\r\nMs. Sandrana Nandini, 3rd Year, B.Tech. (NA&OE)\r\nMs. Aditi Wadekar, 3rd Year, B.Tech. (NA&OE)\r\nMr. Adarsh Ranjan, 3rd Year, B.Tech. (NA&OE)\r\nAchievement: Won the First prize for their Technical paper presentation titled \"Hybrid propulsion a step to green future\" in the technical fest Transtech 2022.\r\nOrganized By: Tolani Maritime Institute, Induri, Pune.\r\n\r\nCongratulations to the students for their achievement in the event','image','img_dynamic/Transtech_Vizag_NAOE3_08042022.png','yes',0,0,6,'2022-04-08',NULL,NULL,'2022-04-21 01:54:29','2022-04-21 01:54:29','active'),(32,5,'AIMA Student Management Quiz Competition','yes','no','sub',27,'yes','Name of the Students:\r\nMs.Shreya Raju\r\nMr. Vikas Raju\r\nAchievement: Emerged as National Champions at the Student Management Quiz Competition held on 31.03.2022.\r\nOrganized By: All India Management Assoication (AIMA)\r\n\r\nVice Chancellor\'s Congratulatory Message:\r\n\r\nCongratulations to Shreya & Vikas for their excellent performance.','image','img_dynamic/Vizag_AIMA_01042022.png','yes',0,0,4,'2022-04-01',NULL,NULL,'2022-04-21 01:59:31','2022-04-21 01:59:31','active'),(33,21,'Scholarships','yes','no','main',0,'yes','State wise post matric scholarship details','pdf','img_dynamic/linkSCHOLARSHIPS.pdf','yes',0,0,3,NULL,NULL,NULL,'2022-04-21 02:49:53','2022-04-21 02:49:53','active'),(34,1,'Research Programmes','yes','no','sub',2,'yes','PhD/MS(By Research) Programme Batch III Details','pdf','img_dynamic/PhDMS(By Research) Programme Batch III Details_220321.pdf','yes',0,0,6,NULL,NULL,NULL,'2022-04-21 09:27:00','2022-04-21 09:27:00','active'),(35,1,'Research Programmes','yes','no','sub',2,'yes','Click here to view list of Internal Guides','pdf','img_dynamic/Internal faculty with profiles_27092021.pdf','yes',0,0,5,NULL,NULL,NULL,'2022-04-21 09:28:31','2022-04-21 09:28:31','active'),(36,1,'Research Programmes','yes','no','sub',2,'yes','PhD/MS(By Research) Programme Batch II Details','pdf','img_dynamic/PhDMS(By Research) Programme Batch II Details_220321.pdf','yes',0,0,4,NULL,NULL,NULL,'2022-04-21 09:29:45','2022-04-21 09:29:45','active');
/*!40000 ALTER TABLE `dynamic_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_management`
--

DROP TABLE IF EXISTS `menu_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_management` (
  `menu_id` int(50) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_description` varchar(255) NOT NULL,
  `menu_type` enum('main','sub') NOT NULL,
  `page_management_parent_id` int(50) NOT NULL,
  `mainmenu_parent_id` int(50) NOT NULL,
  `user_parent_id` int(50) NOT NULL,
  `branch_parent_id` int(100) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `menu_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_management`
--

LOCK TABLES `menu_management` WRITE;
/*!40000 ALTER TABLE `menu_management` DISABLE KEYS */;
INSERT INTO `menu_management` VALUES (1,'About_IMU','','main',1,0,0,2,'2022-04-21 06:39:26','2022-04-21 06:39:26','active'),(2,'Student\'s Ocean','','main',9,0,0,5,'2022-04-21 06:45:14','2022-04-21 06:45:14','active'),(3,'Courses','','sub',2,2,0,4,'2022-04-21 06:46:30','2022-04-21 06:46:30','active'),(4,'Scholarship','','sub',3,2,0,5,'2022-04-21 06:48:05','2022-04-21 06:48:05','active'),(5,'Events','','sub',4,2,0,1,'2022-04-21 06:49:21','2022-04-21 06:49:21','active'),(6,'Brochures','','sub',5,2,0,6,'2022-04-21 06:49:48','2022-04-21 06:49:48','active'),(7,'FAQ','','sub',6,2,0,5,'2022-04-21 06:50:25','2022-04-21 06:50:25','active'),(8,'Achievements','','sub',7,2,0,3,'2022-04-21 06:53:52','2022-04-21 06:53:52','active'),(9,'Alumni','','sub',8,2,0,5,'2022-04-21 06:54:33','2022-04-21 06:54:33','active');
/*!40000 ALTER TABLE `menu_management` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `module_management`
--

DROP TABLE IF EXISTS `module_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `module_management` (
  `module_id` int(100) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(30) NOT NULL,
  `module_content` longtext NOT NULL,
  `user_parent_id` int(50) NOT NULL,
  `branch_parent_id` int(100) NOT NULL,
  `module_image_path` varchar(50) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `module_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `module_management`
--

LOCK TABLES `module_management` WRITE;
/*!40000 ALTER TABLE `module_management` DISABLE KEYS */;
INSERT INTO `module_management` VALUES (1,'module-courses','course module',0,2,'img_dynamic/flower.jpg','2022-04-20 13:00:06','2022-04-20 13:00:06','active'),(2,'module-events','event module',0,1,'img_dynamic/flower1.jpg','2022-04-20 13:01:48','2022-04-20 13:01:48','active'),(3,'module-brochures','brochure module',0,3,'img_dynamic/flower2.jpg','2022-04-20 13:02:37','2022-04-20 13:02:37','active'),(4,'module-faq','faq module',0,5,'img_dynamic/flower3.jpg','2022-04-20 13:03:27','2022-04-20 13:03:27','active'),(5,'module-achievements','achievements module',0,4,'img_dynamic/flower4.jpg','2022-04-20 13:04:06','2022-04-20 13:04:06','active'),(6,'module-alumni','alumni module',0,6,'img_dynamic/flower5.jpg','2022-04-20 13:04:57','2022-04-20 13:04:57','active'),(7,'module-faculty','faculty module',0,1,'img_dynamic/flower6.jpg','2022-04-20 13:06:14','2022-04-20 13:06:14','active'),(8,'module-tenders','tender module',0,2,'img_dynamic/flower7.jpg','2022-04-20 13:06:53','2022-04-20 13:06:53','active'),(9,'module-circulars','circular module',0,3,'img_dynamic/flower8.jpg','2022-04-20 13:07:55','2022-04-20 13:07:55','active'),(10,'module-albums','album module',0,4,'img_dynamic/flower8.jpg','2022-04-20 13:08:57','2022-04-20 13:08:57','active'),(11,'module-feepayments','feepayment module',0,5,'img_dynamic/flower9.jpg','2022-04-20 13:09:37','2022-04-20 13:09:37','active'),(12,'module-certification','certification module',0,4,'img_dynamic/flower10.jpg','2022-04-20 13:17:07','2022-04-20 13:17:07','active'),(13,'module-placement-director','placement-director module',0,5,'img_dynamic/flower1.jpg','2022-04-20 13:18:06','2022-04-20 13:18:06','active'),(14,'module-placement-record','placement-record module',0,5,'img_dynamic/flower2.jpg','2022-04-20 13:20:34','2022-04-20 13:20:34','active'),(15,'module-results','result module',0,4,'img_dynamic/flower2.jpg','2022-04-20 13:39:02','2022-04-20 13:39:02','active'),(16,'module-notifications','notification module',0,3,'img_dynamic/flower2.jpg','2022-04-20 13:39:34','2022-04-20 13:39:34','active'),(17,'module-convocation','convocation module',0,6,'img_dynamic/flower3.jpg','2022-04-20 13:40:50','2022-04-20 13:40:50','active'),(18,'module-registration','registration module',0,2,'img_dynamic/flower3.jpg','2022-04-20 13:42:46','2022-04-20 13:42:46','active'),(19,'module-timetable','timetable module',0,5,'img_dynamic/flower4.jpg','2022-04-20 13:43:45','2022-04-20 13:43:45','active'),(20,'main-module','module main',0,5,'img_dynamic/flower2.jpg','2022-04-20 13:53:31','2022-04-20 13:53:31','active'),(21,'module-scholarships','scholarship module',0,6,'img_dynamic/flower2.jpg','2022-04-21 08:20:30','2022-04-21 08:20:30','active');
/*!40000 ALTER TABLE `module_management` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_management`
--

DROP TABLE IF EXISTS `page_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_management` (
  `page_management_id` int(100) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) NOT NULL,
  `page_type` enum('template_design','db_design') NOT NULL,
  `page_content` longtext NOT NULL,
  `template_parent_id` int(100) NOT NULL,
  `dynamic_parent_id` int(100) NOT NULL,
  `user_parent_id` int(50) NOT NULL,
  `branch_parent_id` int(100) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `page_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`page_management_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_management`
--

LOCK TABLES `page_management` WRITE;
/*!40000 ALTER TABLE `page_management` DISABLE KEYS */;
INSERT INTO `page_management` VALUES (1,'About_IMU','template_design','The Indian Maritime University is headquartered at Chennai and the new HQ building is built at 300 acres land allotted to IMU at Semmancherry which lies between East Coast Road and Old Mahabalipuram Road (Rajiv Gandhi Salai). The IMU has its regional campuses at Chennai, Mumbai, Kolkata, Visakhapatnam and Cochin. IMU presently offers Diploma, Degree, Postgraduate Diploma and Post Graduate Degree courses in Nautical Science, Marine Engineering, Naval Architecture & Ocean Engineering, Ship Building & Repair, Maritime Management, Doctoral Programmes and Post Sea Courses.\r\n\r\n \r\n\r\nThe Indian Maritime University encompasses under its fold the following seven premier government institutions:\r\n\r\n     (1) National Maritime Academy, Chennai\r\n     (2) T S Chanakya, Mumbai\r\n     (3) Lal Bahadur Shastri College of Advanced Maritime Studies & Research, Mumbai\r\n     (4) Marine Engineering Research Institute, Mumbai\r\n     (5) Marine Engineering Research Institute, Kolkata\r\n     (6) Indian Institute of Port Management, Kolkata\r\n     (7) National Ship Design & Research Centre, Visakhapatnam',1,0,0,5,'2022-04-21 06:25:09','2022-04-21 06:25:09','active'),(2,'courses','db_design','',0,2,0,4,'2022-04-21 06:26:15','2022-04-21 06:26:15','active'),(3,'scholarship','db_design','',0,33,0,6,'2022-04-21 06:29:14','2022-04-21 06:29:14','active'),(4,'events','db_design','',0,8,0,2,'2022-04-21 06:30:15','2022-04-21 06:30:15','active'),(5,'brochures','db_design','',0,20,0,1,'2022-04-21 06:31:32','2022-04-21 06:31:32','active'),(6,'faq','db_design','',0,14,0,2,'2022-04-21 06:32:31','2022-04-21 06:32:31','active'),(7,'achievements','db_design','',0,27,0,3,'2022-04-21 06:33:29','2022-04-21 06:33:29','active'),(8,'alumni','db_design','',0,21,0,5,'2022-04-21 06:34:44','2022-04-21 06:34:44','active'),(9,'student\'s ocean','db_design','',0,0,0,2,'2022-04-21 06:44:50','2022-04-21 06:44:50','active');
/*!40000 ALTER TABLE `page_management` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_management`
--

DROP TABLE IF EXISTS `template_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_management` (
  `template_id` int(100) NOT NULL AUTO_INCREMENT,
  `template_title` varchar(30) NOT NULL,
  `template_image_path` varchar(50) NOT NULL,
  `template_content` longtext NOT NULL,
  `user_parent_id` int(50) NOT NULL,
  `branch_parent_id` int(100) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `template_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_management`
--

LOCK TABLES `template_management` WRITE;
/*!40000 ALTER TABLE `template_management` DISABLE KEYS */;
INSERT INTO `template_management` VALUES (1,'template1','img_dynamic/flower.jpg','template 1 content',0,5,'2022-04-20 11:24:01','2022-04-20 11:24:01','active'),(2,'template2','img_dynamic/flower1.jpg','template2 content',0,4,'2022-04-20 11:25:34','2022-04-20 11:25:34','active'),(3,'template3','img_dynamic/flower2.jpg','template3 content',0,4,'2022-04-20 11:26:06','2022-04-20 11:26:06','active'),(4,'template4','img_dynamic/flower3.jpg','template4 content',0,6,'2022-04-20 11:26:22','2022-04-20 11:26:22','active'),(5,'template5','img_dynamic/flower4.jpg','template5 content',0,3,'2022-04-20 11:27:59','2022-04-20 11:27:59','active'),(6,'template6','img_dynamic/flower5.jpg','template6 content',0,2,'2022-04-20 11:28:43','2022-04-20 11:28:43','active'),(7,'template7','img_dynamic/flower6.jpg','template7 content',0,5,'2022-04-20 11:31:00','2022-04-20 11:31:00','active'),(8,'template8','img_dynamic/flower7.jpg','template8 content',0,1,'2022-04-20 11:31:55','2022-04-20 11:31:55','active'),(9,'template9','img_dynamic/flower8.jpg','template9 content',0,6,'2022-04-20 11:33:54','2022-04-20 11:33:54','active'),(10,'template10','img_dynamic/flower9.jpg','template10 content',0,5,'2022-04-20 11:34:51','2022-04-20 11:34:51','active');
/*!40000 ALTER TABLE `template_management` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_history`
--

DROP TABLE IF EXISTS `user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_history` (
  `history_id` int(100) NOT NULL AUTO_INCREMENT,
  `action_date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `action` enum('insert','update','delete','select','create') NOT NULL,
  `user_management_parent_id` int(50) NOT NULL,
  `user_type_parent_id` int(50) NOT NULL,
  `branch_parent_id` int(100) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_history`
--

LOCK TABLES `user_history` WRITE;
/*!40000 ALTER TABLE `user_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_management`
--

DROP TABLE IF EXISTS `user_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_management` (
  `user_id` int(50) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_mobile_number` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_role_parent_id` int(50) NOT NULL,
  `branch_parent_id` int(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_management`
--

LOCK TABLES `user_management` WRITE;
/*!40000 ALTER TABLE `user_management` DISABLE KEYS */;
INSERT INTO `user_management` VALUES (1,'user1','first1','last1','9523687410','7HZ|&\'<R*L7E&+?&',1,1),(2,'user2','first2','last2','5896328741','B#^L[GJJXD{WC@0F',3,5),(3,'user3','first3','last3','6398721598','PM8%`\"O.$(|*0XI/',2,4),(4,'user4','first4','last4','8456321789','a&&[;s8au9te]p9t',3,2),(5,'user5','first5','last5','9324851269','3m@a!r$p&0.%:cz:',3,3),(6,'user6','first6','last6','9125463894','T&*$(097HF3%',2,6);
/*!40000 ALTER TABLE `user_management` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_permission` (
  `user_permission_id` int(50) NOT NULL AUTO_INCREMENT,
  `user_permission_name` varchar(255) NOT NULL,
  `user_permission_list` enum('select','insert','update','delete','create') NOT NULL,
  `user_role_parent_id` int(50) NOT NULL,
  PRIMARY KEY (`user_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permission`
--

LOCK TABLES `user_permission` WRITE;
/*!40000 ALTER TABLE `user_permission` DISABLE KEYS */;
INSERT INTO `user_permission` VALUES (1,'sadmin_read','select',1),(2,'sadmin_write','insert',1),(3,'sadmin_write','update',1),(4,'sadmin_write','delete',1),(5,'sadmin_write','create',1),(6,'admin_read','select',2),(7,'admin_write','insert',2),(8,'admin_write','update',2),(9,'admin_write','create',2),(10,'otheruser_read','select',3);
/*!40000 ALTER TABLE `user_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_role_id` int(50) NOT NULL AUTO_INCREMENT,
  `user_role` varchar(255) NOT NULL,
  PRIMARY KEY (`user_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'super admin'),(2,'admin'),(3,'other user');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-22 16:42:09
